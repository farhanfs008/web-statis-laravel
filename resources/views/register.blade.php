<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title> Daftar </title>
    </head>

    <body>
        <div>
            <h1>Buat Account Baru!</h1>
            <h2>Sign Up Form</h2>
        </div>

        <!--Data Diri-->
        <form action="welcome" method="POST">
        @csrf
            <label for="first_name_user">First Name:</label> <br> <br>
            <input type="text" id="first_name_user" name="namadepan"> <br> <br>
            <label for="last_name_user" name="namabelakang">Last Name:</label> <br><br>
            <input type="text" id="last_name_user"> <br> <br>
            <label>Gender</label> <br> <br>
            <input type="radio" name="gender" value="Male">Male <br>
            <input type="radio" name="gender" value="Female">Female <br>
            <input type="radio" name="gender" value="Other">Other <br> <br>
            <label>Nationality:</label> <br> <br>
            <select>
                <option value="id">Indonesian</option>
                <option value="sg">Singaporean</option>
                <option value="my">Malaysian</option>
                <option value="au">Australian</option>
            </select> <br> <br>
            <label for="language_spoken_user">Language Spoken:</label> <br> <br>
            <input type="checkbox" name="language_spoken_user" value="Bahasa Indonesia"> Bahasa Indonesia <br>
            <input type="checkbox" name="language_spoken_user" value="English"> English <br>
            <input type="checkbox" name="language_spoken_user" value="Arabic"> Arabic <br>
            <input type="checkbox" name="language_spoken_user" value="Japanese"> Japanese <br><br>
            <label for="bio_user">Bio:</label> <br> <br>
            <textarea cols="30" rows="10" id="bio_user"></textarea> <br>
            <input type="submit" value="Sign Up">
            
        </form>
    </body>
</html>